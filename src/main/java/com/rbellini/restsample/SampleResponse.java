package com.rbellini.restsample;

import org.springframework.hateoas.ResourceSupport;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;


public class SampleResponse extends ResourceSupport {
    private final String message;

    @JsonCreator
    public SampleResponse(@JsonProperty("message") String msg) {
        this.message = msg;
    }

    public String getMessage() {
        return this.message;
    }
}
