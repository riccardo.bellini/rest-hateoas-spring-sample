package com.rbellini.restsample.security;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import com.microsoft.aad.adal4j.AuthenticationContext;
import com.microsoft.aad.adal4j.AuthenticationResult;
import com.microsoft.aad.adal4j.ClientCredential;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.microsoft.azure.keyvault.authentication.KeyVaultCredentials;
import com.microsoft.rest.credentials.ServiceClientCredentials;

public class KeyVaultAuthenticator {

    public static KeyVaultClient authenticateClient() {
        return new KeyVaultClient(getCredentials());
    }

    private static ServiceClientCredentials getCredentials() {
        return new KeyVaultCredentials(){
        
            @Override
            public String doAuthenticate(String authorization, String resource, String scope) {
                AuthenticationResult result = null;
                try {
                    result = generateNewAccessToken(authorization, resource);
                    return result.getAccessToken();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return "";
            }
        };
    }

    private static AuthenticationResult generateNewAccessToken(String authority, String resource) {

        String clientId = System.getenv("AZURE_CLIENT_ID");
        String clientKey = System.getenv("AZURE_CLIENT_SECRET");

        AuthenticationResult result = null;

        ExecutorService srv = null;
        try {
            srv = Executors.newSingleThreadExecutor();

            AuthenticationContext authCtx = new AuthenticationContext(authority, false, srv);
            Future<AuthenticationResult> future = null;
            if (clientId != null && clientKey != null) {
                future = authCtx.acquireToken(resource, new ClientCredential(clientId, clientKey), null);
            }

            result = future.get();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (srv != null && !srv.isShutdown()) {
                srv.shutdown();
            }
        }

        if (result == null) {
			throw new RuntimeException("Authentication results were null.");
        }
        return result;
    }

}