package com.rbellini.restsample;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.rbellini.restsample.model.CustomerDataItem;
import com.rbellini.restsample.service.CustomerService;

@RestController
public class SampleController {
    
    @Autowired
    private CustomerService customerSrv;
    
    private static final String TEMPLATE = "Welcome to this RESTful API, %s!";

    @RequestMapping(path="/hello", method = RequestMethod.GET)
    public HttpEntity<SampleResponse> sayHello(@RequestParam(value = "name", required = false, defaultValue = "John Doe") String name) {

        SampleResponse sResp = new SampleResponse(String.format(TEMPLATE, name));
        sResp.add(linkTo(methodOn(SampleController.class).sayHello(name)).withSelfRel());
        return new ResponseEntity<SampleResponse>(sResp, HttpStatus.OK);
    }
    
    @RequestMapping(path="/customers", method = RequestMethod.GET)
    public HttpEntity<Collection<CustomerDataItem>> getCustomers() {
        Collection<CustomerDataItem> coll = customerSrv.getAll();
        return new ResponseEntity<Collection<CustomerDataItem>>(coll, HttpStatus.OK);
    }
}