package com.rbellini.restsample.model;

import java.time.Instant;

public class CustomerDataItem {
	private String name = "";
	private int msgCount = 0;
	private Instant detectionTime = null;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getMsgCount() {
		return msgCount;
	}

	public void setMsgCount(int msgCount) {
		this.msgCount = msgCount;
	}

	public Instant getDetectionTime() {
		return detectionTime;
	}

	public void setDetectionTime(Instant detectionTime) {
		this.detectionTime = detectionTime;
	}
}