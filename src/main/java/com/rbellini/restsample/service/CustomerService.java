package com.rbellini.restsample.service;

import java.util.Collection;
import java.util.Optional;

import com.rbellini.restsample.model.CustomerDataItem;

public interface CustomerService {

    public Collection<CustomerDataItem> getAll();
    
    public Optional<CustomerDataItem> getById(String id);
    
}
