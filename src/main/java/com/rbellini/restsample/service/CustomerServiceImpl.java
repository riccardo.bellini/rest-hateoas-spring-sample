package com.rbellini.restsample.service;

import java.util.Collection;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rbellini.restsample.cosmosdb.repository.CustomerDataRepository;
import com.rbellini.restsample.model.CustomerDataItem;

@Service
public class CustomerServiceImpl implements CustomerService {

    @Autowired
    private CustomerDataRepository repo;
    
    public Collection<CustomerDataItem> getAll() {
        return repo.getCustomerDataList();
    }
    
    public Optional<CustomerDataItem> getById(String id) {
        return repo.getCustomerData(id);
    }
    
}
