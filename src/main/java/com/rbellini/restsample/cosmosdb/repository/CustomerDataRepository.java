package com.rbellini.restsample.cosmosdb.repository;

import java.util.Collection;
import java.util.Optional;

import com.rbellini.restsample.model.CustomerDataItem;

public interface CustomerDataRepository {

    public Collection<CustomerDataItem> getCustomerDataList();
    
    public Optional<CustomerDataItem> getCustomerData(String id);
}
