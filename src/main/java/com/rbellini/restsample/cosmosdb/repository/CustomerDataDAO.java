package com.rbellini.restsample.cosmosdb.repository;

import java.time.Instant;
import java.util.Collection;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Repository;

import com.microsoft.azure.documentdb.Database;
import com.microsoft.azure.documentdb.Document;
import com.microsoft.azure.documentdb.DocumentCollection;
import com.rbellini.restsample.cosmosdb.CosmosDocumentClientFactory;
import com.rbellini.restsample.model.CustomerDataItem;

@Repository
public class CustomerDataDAO implements CustomerDataRepository {
    
    private static final String DATABASE_ID = "cosmos-db";
    private static final String COLLECTION_ID = "raccolta-cosmos";
    
    private static Database databaseCache;
    private static DocumentCollection collectionCache;

    public Collection<CustomerDataItem> getCustomerDataList() {
        List<Document> docList = CosmosDocumentClientFactory.getDocumentClient()
                .queryDocuments(getCollection().getSelfLink(),
                        "SELECT * FROM r", null).getQueryIterable().toList();
        
        List<CustomerDataItem> result = docList.stream().map((doc) -> {
            CustomerDataItem newObj = new CustomerDataItem();
            newObj.setName(doc.getString("customer"));
            newObj.setMsgCount(doc.getInt("count"));
            newObj.setDetectionTime(Instant.parse(doc.getString("detectiontime")));
            return newObj;
        }).collect(Collectors.toList());
        
        return result;
    }

    public Optional<CustomerDataItem> getCustomerData(String id) {
        // TODO Auto-generated method stub
        return null;
    }
    
    private static Database getDatabase() {
        if (databaseCache == null) {
            databaseCache = _retrieveDbCache();
        }
        return databaseCache;
    }
    
    private static DocumentCollection getCollection() {
        if (collectionCache == null) {
            collectionCache = _retrieveCollectionCache();
        }
        return collectionCache;
    }
    
    private static DocumentCollection _retrieveCollectionCache() {
        List<DocumentCollection> collectionList = CosmosDocumentClientFactory.getDocumentClient()
                .queryCollections(getDatabase().getSelfLink(), "SELECT * FROM root r WHERE r.id='" + COLLECTION_ID + "'", null)
                .getQueryIterable().toList();
        if (collectionList.size() > 0) {
            collectionCache = collectionList.get(0);
        } else {
            throw new IllegalStateException("Collection " + COLLECTION_ID + " not found!");
        }
        return collectionCache;
    }

    private static Database _retrieveDbCache() {
        List<Database> databaseList = CosmosDocumentClientFactory.getDocumentClient()
                .queryDatabases("SELECT * FROM root r WHERE r.id='" + DATABASE_ID + "'", null)
                .getQueryIterable().toList();
        if (databaseList.size() > 0) {
            databaseCache = databaseList.get(0);
        } else {
            throw new IllegalStateException("Database " + DATABASE_ID + " not found!");
        }
        return databaseCache;
    }

}
