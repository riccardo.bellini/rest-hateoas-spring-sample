package com.rbellini.restsample.cosmosdb;

import com.microsoft.azure.documentdb.ConnectionPolicy;
import com.microsoft.azure.documentdb.ConsistencyLevel;
import com.microsoft.azure.documentdb.DocumentClient;
import com.microsoft.azure.keyvault.KeyVaultClient;
import com.rbellini.restsample.security.KeyVaultAuthenticator;

public class CosmosDocumentClientFactory {

    private static DocumentClient instance = null;
    
    private static String HOSTNAME_ENV = "AZURE_COSMOS_DB_URI";
    private static String PRIMARY_KEY_ENV = "AZURE_COSMOS_DB_KEY";
    
    private CosmosDocumentClientFactory() {}
    
    public static DocumentClient getDocumentClient() {
        if (instance == null) {
            // try to instantiate key vault client
            KeyVaultClient client = KeyVaultAuthenticator.authenticateClient();
            String hostname = client.getSecret(System.getenv(HOSTNAME_ENV)).value();
            String dbKey = client.getSecret(System.getenv(PRIMARY_KEY_ENV)).value();
            ConnectionPolicy cp = ConnectionPolicy.GetDefault();
            cp.setEnableEndpointDiscovery(false);
            instance = new DocumentClient(
                    hostname,
                    dbKey,
                    cp,
                    ConsistencyLevel.Session);
        }
        return instance;
    }
}
